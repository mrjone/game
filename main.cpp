#include <iostream>
#include <windows.h>

#define COUNT_WALLS 3

using namespace std;

int main()
{
    WORD pawn, wall;
    pawn = FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_INTENSITY
        | BACKGROUND_RED | BACKGROUND_INTENSITY;
    wall = FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_INTENSITY
        | BACKGROUND_GREEN | BACKGROUND_INTENSITY;

    COORD *walls;
    walls = new COORD[COUNT_WALLS];

    for (int i = 0; i < COUNT_WALLS; i++)
    {
        cin >> walls[i].X >> walls[i].Y;
    }

    COORD pos, consoleSize;
    HANDLE hConsole;
    CONSOLE_CURSOR_INFO curInfo;
    CONSOLE_SCREEN_BUFFER_INFO consoleInfo;
    curInfo.bVisible = false;
    curInfo.dwSize = 100;
    hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
    SetConsoleCursorInfo(hConsole,
                         &curInfo);
    SetConsoleTitle("Super GAME");
    GetConsoleScreenBufferInfo(hConsole,
                               &consoleInfo);
    consoleInfo.dwSize.Y = 30;
    SetConsoleScreenBufferSize(hConsole,
                               consoleInfo.dwSize);
    consoleSize.X = consoleInfo.dwSize.X;
    consoleSize.Y = consoleInfo.dwSize.Y;
    pos.X = 30;
    pos.Y = 15;
    SetConsoleCursorPosition(hConsole, pos);
    bool isRun = true;
    int oldStateUP = 0,
        oldStateDOWN = 0,
        oldStateLEFT = 0,
        oldStateRIGHT = 0;
    int eventUP, eventDOWN, eventLEFT, eventRIGHT;

    while(isRun)
    {
        SetConsoleTextAttribute(hConsole, wall);
        for (int i = 0; i < 3; i++)
        {
            SetConsoleCursorPosition(hConsole, walls[i]);
            putchar('#');
        }
        SetConsoleTextAttribute(hConsole, consoleInfo.wAttributes);

        if (GetKeyState(VK_ESCAPE) & 0x8000)
        {
            isRun = false;
        }

        //KEY UP BEGIN
        if ((GetKeyState(VK_UP)& 0x8000) && oldStateUP == 0)
        {
            eventUP = 1;
            oldStateUP = 1;
        }
        if (!(GetKeyState(VK_UP) & 0x8000) && oldStateUP == 1)
        {
            eventUP = 2;
            oldStateUP = 0;
        }
        //KEY UP END
        //KEY DOWN BEGIN
        if ((GetKeyState(VK_DOWN)& 0x8000) && oldStateDOWN == 0)
        {
            eventDOWN = 1;
            oldStateDOWN = 1;
        }
        if (!(GetKeyState(VK_DOWN) & 0x8000) && oldStateDOWN == 1)
        {
            eventDOWN = 2;
            oldStateDOWN = 0;
        }
        //KEY DOWN END
        //KEY LEFT BEGIN
        if ((GetKeyState(VK_LEFT)& 0x8000) && oldStateLEFT == 0)
        {
            eventLEFT = 1;
            oldStateLEFT = 1;
        }
        if (!(GetKeyState(VK_LEFT) & 0x8000) && oldStateLEFT == 1)
        {
            eventLEFT = 2;
            oldStateLEFT = 0;
        }
        //KEY LEFT END
        //KEY RIGHT BEGIN
        if ((GetKeyState(VK_RIGHT)& 0x8000) && oldStateRIGHT == 0)
        {
            eventRIGHT = 1;
            oldStateRIGHT = 1;
        }
        if (!(GetKeyState(VK_RIGHT) & 0x8000) && oldStateRIGHT == 1)
        {
            eventRIGHT = 2;
            oldStateRIGHT = 0;
        }
        //KEY RIGHT END

        switch(eventUP)
        {
        case 1:
            {
                //cout << "Pressed" << endl;
                for (int i = 0; i < COUNT_WALLS; i++)
                {
                    if (pos.Y == 0)
                    {
                        if (walls[i].Y == consoleSize.Y-1
                            && walls[i].X == pos.X)
                        {
                            isRun = false;
                        }
                    }
                    else
                    {
                        if (walls[i].Y == pos.Y-1
                            && walls[i].X == pos.X)
                        {
                            isRun = false;
                        }
                    }
                }

                SetConsoleCursorPosition(hConsole,
                                         pos);
                putchar(' ');
                if (pos.Y == 0)
                    pos.Y = consoleSize.Y-1;
                else
                    pos.Y--;
                SetConsoleTextAttribute(hConsole, pawn);
                SetConsoleCursorPosition(hConsole,
                                         pos);
                putchar('0');
                SetConsoleTextAttribute(hConsole, consoleInfo.wAttributes);
                eventUP = 0;
                break;
            }
        case 2:
            {
                //cout << "Release" << endl;
                eventUP = 0;
                break;
            }
        }

        switch(eventDOWN)
        {
        case 1:
            {
                for (int i = 0; i < COUNT_WALLS; i++)
                {
                    if (pos.Y == consoleSize.Y-1)
                    {
                        if (walls[i].Y == 0
                            && walls[i].X == pos.X)
                        {
                            isRun = false;
                        }
                    }
                    else
                    {
                        if (walls[i].Y == pos.Y+1
                            && walls[i].X == pos.X)
                        {
                            isRun = false;
                        }
                    }
                }

                SetConsoleCursorPosition(hConsole,
                                             pos);
                putchar(' ');
                if (pos.Y == consoleSize.Y-1)
                    pos.Y = 0;
                else
                    pos.Y++;

                SetConsoleTextAttribute(hConsole, pawn);
                SetConsoleCursorPosition(hConsole,
                                         pos);
                putchar('0');
                SetConsoleTextAttribute(hConsole, consoleInfo.wAttributes);
                eventDOWN = 0;
                break;
            }
        case 2:
            eventDOWN = 0;
            break;
        }

        switch(eventLEFT)
        {
        case 1:
            {
                //cout << "Pressed" << endl;
                for (int i = 0; i < COUNT_WALLS; i++)
                {
                    if (pos.X == 0)
                    {
                        if (walls[i].X == consoleSize.X-1
                            && walls[i].Y == pos.Y)
                        {
                            isRun = false;
                        }
                    }
                    else
                    {
                        if (walls[i].X == pos.X-1
                            && walls[i].Y == pos.Y)
                        {
                            isRun = false;
                        }
                    }
                }

                SetConsoleCursorPosition(hConsole,
                                         pos);
                putchar(' ');
                if (pos.X == 0)
                    pos.X = consoleSize.X-1;
                else
                    pos.X--;
                SetConsoleTextAttribute(hConsole, pawn);
                SetConsoleCursorPosition(hConsole,
                                         pos);
                putchar('0');
                SetConsoleTextAttribute(hConsole, consoleInfo.wAttributes);
                eventLEFT = 0;
                break;
            }
        case 2:
            {
                //cout << "Release" << endl;
                eventLEFT = 0;
                break;
            }
        }
        switch(eventRIGHT)
        {
        case 1:
            {
                //cout << "Pressed" << endl;
                for (int i = 0; i < COUNT_WALLS; i++)
                {
                    if (pos.X == consoleSize.X-1)
                    {
                        if (walls[i].X == 0
                            && walls[i].Y == pos.Y)
                        {
                            isRun = false;
                        }
                    }
                    else
                    {
                        if (walls[i].X == pos.X+1
                            && walls[i].Y == pos.Y)
                        {
                            isRun = false;
                        }
                    }
                }

                SetConsoleCursorPosition(hConsole,
                                         pos);
                putchar(' ');
                if (pos.X == consoleSize.X-1)
                    pos.X = 0;
                else
                    pos.X++;
                SetConsoleTextAttribute(hConsole, pawn);
                SetConsoleCursorPosition(hConsole,
                                         pos);
                putchar('0');
                SetConsoleTextAttribute(hConsole, consoleInfo.wAttributes);
                eventRIGHT = 0;
                break;
            }
        case 2:
            {
                //cout << "Release" << endl;
                eventRIGHT = 0;
                break;
            }
        }
    }
    return 0;
}
